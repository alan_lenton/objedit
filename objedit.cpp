/*-----------------------------------------------------------------------
ObjEdit version 2.00 - Windows Federation 2 Object Editor
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "objedit.h"

#include <QCloseEvent>
#include <QDesktopServices>
#include <QFileDialog>
#include <QFontDialog>
#include <QMessageBox>
#include <QSettings>
#include <QStringList>
#include <QTableWidget>
#include <QUrl>

#include <sstream>

#include <ctime>

#include "obj_file.h"
#include "object.h"


ObjEdit		*ObjEdit::main_window = 0;
const int	ObjEdit::TABLE_ROWS = 20;

ObjEdit::ObjEdit(QWidget *parent, Qt::WindowFlags flags) : QMainWindow(parent, flags)
{
	ui.setupUi(this);

	current = 0;
	obj_file = new ObjFile;
	version = "Version 2.01";
	std::ostringstream	buffer;
	buffer << "ObjEdit - Federation 2 Object Editor - " << version;
	setWindowTitle(buffer.str().c_str());

	CreateActions();
	CreateMainMenu();
	CreateStatusBar();
	SetUpConnections();
	SetUpTables();

	QCoreApplication::setOrganizationName("ibgames");
	QCoreApplication::setOrganizationDomain("ibgames.com");
	QCoreApplication::setApplicationName("ObjEdit");

	QSettings settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "ObjEdit");
	QFont	font(settings.value("font-type", "verdana").toString(), settings.value("font-size", 8).toInt());
	ui.centralWidget->setFont(font);
	resize(settings.value("win-size", QSize(780, 540)).toSize());
	move(settings.value("win-pos", QPoint(20, 20)).toPoint());
	current_path = settings.value("cur-dir", "./").toString().toStdString();

	changed = false;
	main_window = this;
}

ObjEdit::~ObjEdit()
{
	QSettings settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "ObjEdit");
	settings.setValue("font-type", ui.centralWidget->font().family());
	settings.setValue("font-size", ui.centralWidget->font().pointSize());
	settings.setValue("win-size", size());
	settings.setValue("win-pos", pos());
	settings.setValue("cur-dir", QString::fromStdString(current_path));
	delete obj_file;
}


void	ObjEdit::About()
{
	QString	buffer = "Federation 2 Object Editor\n";
	buffer += "Copyright (c) 1985-2013 Alan Lenton\n";
	buffer += "Released under the GNU General Public License v3\n";
	buffer += "http://www.gnu.org/copyleft/gpl.html\n\n";
	buffer += "Design and programming by Alan Lenton\n";
	buffer += "Testing and manuals by Fiona Craig\n";
	buffer += "Website: www.ibgames.com\n";
	buffer += "Email: feedback@ibgames.com\n\n";
	buffer += "git: https://bitbucket.org/alanxxx/objeditor/overview";
	QMessageBox::information(this, "About Federation 2 Object Editor", buffer);
}

void	ObjEdit::AddObjectToList(const Object *object)
{
	std::ostringstream	buffer;
	buffer << object->Id() << " [" << object->Name() << "]";
	ui.object_list->addItem(QString::fromStdString(buffer.str()));
}

bool	ObjEdit::AskAboutFileSave()
{
	std::string	question("Not all objects have been saved to disk.\nDo you want to continue?");
	if (QMessageBox::question(this, "Unsaved objects", QString::fromStdString(question),
		QMessageBox::Yes | QMessageBox::Cancel) == QMessageBox::Yes)
		return(true);
	else
		return(false);
}

bool	ObjEdit::CheckForSave()
{
	if (!changed)
		return(true);

	std::string	question("You will lose the edits on the\ncurrent object. Do you want to continue?");
	if (QMessageBox::question(this, "Unsaved editing", QString::fromStdString(question),
		QMessageBox::Yes | QMessageBox::Cancel) == QMessageBox::Yes)
		return(true);
	else
		return(false);
}

void	ObjEdit::ClearObjectDisplay()
{
	// General
	ui.name_edit->setText("");
	ui.id_edit->setText("");
	ui.desc_edit->clear();

	// Characteristics
	ui.start_edit->setText("0");
	ui.size_edit->setText("1");
	ui.weight_edit->setText("1");
	ui.visible_check->setCheckState(Qt::Checked);
	ui.definite_btn->setChecked(false);
	ui.indef_btn->setChecked(true);
	ui.plural_btn->setChecked(false);
	ui.no_art_btn->setChecked(false);
	ui.persist_check->setCheckState(Qt::Checked);
	ui.no_teleport_check->setCheckState(Qt::Unchecked);
	ui.abstract_btn->setChecked(true);
	OnNonStaticBtnClicked(true);
	ui.dynamic_btn->setChecked(false);
	ui.mobile_btn->setChecked(false);
	ui.static_btn->setChecked(false);

	// Vocabulary
	ui.vocab_table->clear();
	QStringList	header_list("Command");
	header_list.append("Event");
	ui.vocab_table->setHorizontalHeaderLabels(header_list);

	current = 0;
	changed = false;
}

void	ObjEdit::ClearObjectList()
{
	ui.object_list->clear();
}

void	ObjEdit::Close()
{
	if (!CheckForSave())
		return;
	if (!obj_file->CanClose())
	{
		if (!AskAboutFileSave())
			return;
	}
	close();
}

void	ObjEdit::closeEvent(QCloseEvent *event)
{
	if (!CheckForSave())
	{
		event->ignore();
		return;
	}
	if (!obj_file->CanClose())
	{
		if (!AskAboutFileSave())
		{
			event->ignore();
			return;
		}
	}
	event->accept();
}

void	ObjEdit::CreateActions()
{
	about_action = new QAction("About", this);
	about_action->setStatusTip("Copyright and version details");

	del_obj_action = new QAction("Delete Object", this);
	ui.object_list->addAction(del_obj_action);
	ui.object_list->setContextMenuPolicy(Qt::ActionsContextMenu);

	exit_action = new QAction("E&xit", this);
	exit_action->setShortcut(tr("Ctrl+x"));
	exit_action->setStatusTip("Close the object editor");

	font_action = new QAction("Change Font", this);
	font_action->setStatusTip("Change font size or family");

	load_action = new QAction("&Load", this);
	load_action->setShortcut(tr("Ctrl+l"));
	load_action->setStatusTip("Load in a new file of objects");

	help_action = new QAction("Help", this);
	help_action->setStatusTip("Access on-line help");

	new_file_action = new QAction("&New File", this);
	new_file_action->setShortcut(tr("Ctrl+n"));
	new_file_action->setStatusTip("Start a new file of objects");

	new_obj_action = new QAction("New Object", this);
	new_obj_action->setStatusTip("Clear the screen for a new object");

	save_action = new QAction("&Save", this);
	save_action->setShortcut(tr("Ctrl+s"));
	save_action->setStatusTip("Save objects to disk file");
	save_action->setEnabled(false);

	saveas_action = new QAction("Save As...", this);
	saveas_action->setStatusTip("Save objects to a different disk file");

	undo_action = new QAction("Undo Object Delete", this);
	undo_action->setStatusTip("Undo the last deletion of an object");
	undo_action->setEnabled(false);
}

void	ObjEdit::CreateMainMenu()
{
	file_menu = menuBar()->addMenu("File");
	file_menu->addAction(new_file_action);
	file_menu->addSeparator();
	file_menu->addAction(load_action);
	file_menu->addAction(save_action);
	file_menu->addAction(saveas_action);
	file_menu->addSeparator();
	file_menu->addAction(exit_action);

	edit_menu = menuBar()->addMenu("Edit");
	edit_menu->addAction(undo_action);
	edit_menu->addSeparator();
	edit_menu->addAction(new_obj_action);
	edit_menu->addSeparator();
	edit_menu->addAction(font_action);

	help_menu = menuBar()->addMenu("Help");
	help_menu->addAction(help_action);
	edit_menu->addSeparator();
	help_menu->addAction(about_action);
}

void	ObjEdit::CreateStatusBar()
{
	status_label = new QLabel(this);
	statusBar()->addPermanentWidget(status_label);
	statusBar()->showMessage(QString::fromLatin1("Ready..."));
}

void	ObjEdit::DeleteObject()
{
	QListWidgetItem *item = ui.object_list->currentItem();
	std::string	text(item->text().toStdString());
	std::string::size_type	index = text.find(" [");
	if (index != std::string::npos)
	{
		std::string	id(text.substr(0, index));
		std::ostringstream	buffer;
		buffer << "Delete object with ID " << id << ".\nAre you sure?";
		if (QMessageBox::question(this, "Delete Object", QString::fromStdString(buffer.str()),
			QMessageBox::Yes | QMessageBox::Cancel) == QMessageBox::Yes)
		{
			obj_file->RemoveObject(id);
		}
	}
}

void	ObjEdit::Display(Object *object)
{
	ClearObjectDisplay();
	current = object;

	// General
	ui.name_edit->setText(QString::fromStdString(current->Name()));
	ui.id_edit->setText(QString::fromStdString(current->Id()));
	ui.desc_edit->append(QString::fromStdString(current->Desc()));

	// Characteristics
	ui.start_edit->setText(QString::fromStdString(current->StartLoc()));
	ui.size_edit->setText(QString::fromStdString(current->Size()));
	ui.weight_edit->setText(QString::fromStdString(current->Weight()));
	if (current->Visibility() == "100")
		ui.visible_check->setCheckState(Qt::Checked);
	else
		ui.visible_check->setCheckState(Qt::Unchecked);

	if ((!current->Property(Object::DEFINITE)) && (!current->Property(Object::INDEFINITE))
		&& (!current->Property(Object::PLURAL)))
	{
		ui.no_art_btn->setChecked(true);
	}
	else
	{
		ui.definite_btn->setChecked(current->Property(Object::DEFINITE));
		ui.indef_btn->setChecked(current->Property(Object::INDEFINITE));
		ui.plural_btn->setChecked(current->Property(Object::PLURAL));
	}

	if (current->Property(Object::STORABLE))
		ui.persist_check->setCheckState(Qt::Checked);
	else
		ui.persist_check->setCheckState(Qt::Unchecked);
	if (current->Property(Object::NO_TP))
		ui.no_teleport_check->setCheckState(Qt::Checked);
	else
		ui.no_teleport_check->setCheckState(Qt::Unchecked);
	switch (current->ObjectType())
	{
	case Object::OB_ABSTRACT:
		ui.abstract_btn->setChecked(true);
		OnNonStaticBtnClicked(true);
		break;
	case Object::OB_DYNAMIC:
		ui.dynamic_btn->setChecked(true);
		OnNonStaticBtnClicked(true);
		break;
	case Object::OB_MOBILE:		ui.mobile_btn->setChecked(true);		break;
	case Object::OB_STATIC:
		ui.static_btn->setChecked(true);
		OnStaticBtnClicked(true);
		break;
	}

	// Vocabulary
	int count = 0;
	QTableWidgetItem *item;
	const VocabList& vocab_list = current->Vocab();
	for (VocabList::const_iterator iter = vocab_list.begin();
		(count < TABLE_ROWS) && (iter != vocab_list.end()); ++iter, ++count)
	{
		item = new QTableWidgetItem(QString::fromStdString((*iter)->command));
		ui.vocab_table->setItem(count, COMMAND, item);
		item = new QTableWidgetItem(QString::fromStdString((*iter)->event));
		ui.vocab_table->setItem(count, EVENT, item);
	}
	// Adding entries seems to reset the headers, so put them back :(
	QStringList	header_list("Command");
	header_list.append("Event");
	ui.vocab_table->setHorizontalHeaderLabels(header_list);

	/****************** mobile stuff will go in here eventually ******************/

	changed = false;
}

void	ObjEdit::DisplayObjectList(const std::map<std::string, Object *, std::less<std::string> >& index)
{
	ui.object_list->clear();
	std::ostringstream	buffer;
	for (ObjectIndex::const_iterator iter = index.begin(); iter != index.end(); ++iter)
	{
		buffer.str("");
		buffer << iter->first << " [" << iter->second->Name() << "]";
		ui.object_list->addItem(QString::fromStdString(buffer.str()));
	}
}

void	ObjEdit::Error(const std::string& text)
{
	QMessageBox::critical(this, tr("ObjEdit"), QString::fromStdString(text));
}

void	ObjEdit::Font()
{
	bool ok;
	QFont	original_font(ui.centralWidget->font());
	QFont font = QFontDialog::getFont(&ok, ui.centralWidget->font(), this);
	if (!ok)
		font = original_font;
	ui.centralWidget->setFont(font);
}

void	ObjEdit::Help()
{
	QString	buffer("http://www.ibgames.net/fed2/workbench/advanced/index.html");
	QDesktopServices::openUrl(buffer);
}

void	ObjEdit::Load()
{
	if (!CheckForSave())
		return;

	QString	file_name = QFileDialog::getOpenFileName(this, tr("Load File"),
		QString::fromStdString(current_path), tr("Object files (*.xob);;All files (*.*"));
	if (!file_name.isNull())
	{
		obj_file->Load(file_name.toStdString());
		QFileInfo	file_info(QString::fromStdString(obj_file->FileName()));
		current_path = (file_info.absoluteDir()).absolutePath().toStdString();
	}
}

void	ObjEdit::Message(const std::string& text)
{
	QMessageBox::information(this, tr("ObjEdit"), QString::fromStdString(text));
}

void	ObjEdit::NewFile()
{
	if (!CheckForSave())
		return;

	obj_file->NewFile();
	current = 0;
}

void	ObjEdit::NewObject()
{
	if (!CheckForSave())
		return;

	current = 0;
	ui.object_list->setCurrentRow(-1);
}

void	ObjEdit::OnAddBtnClicked(bool checked)
{
	Object *obj = new Object();
	UpdateObject(obj);
	if (obj->Test())
	{
		if (obj_file->AddObject(obj))
		{
			changed = false;
			ui.object_list->setCurrentRow(-1);
		}
	}
}

void	ObjEdit::OnNonStaticBtnClicked(bool checked)
{
	ui.persist_check->setEnabled(true);
	ui.no_teleport_check->setEnabled(true);
	changed = true;
}

void	ObjEdit::OnObjectSelect()
{
	if (!CheckForSave())
		return;  /****************** hmmmm need to figure out what to do here ************************/

	QListWidgetItem *item = ui.object_list->currentItem();
	if (item != 0)
	{
		std::string	text(item->text().toStdString());
		std::string::size_type	index = text.find(" [");
		if (index != std::string::npos)
		{
			std::string	id(text.substr(0, index));
			obj_file->ListSelected(id);
		}
	}
	else
		ClearObjectDisplay();
}

void	ObjEdit::OnStaticBtnClicked(bool checked)
{
	ui.persist_check->setChecked(false);
	ui.persist_check->setEnabled(false);
	ui.no_teleport_check->setChecked(false);
	ui.no_teleport_check->setEnabled(false);
	changed = true;
}

void	ObjEdit::OnUpdateBtnClicked(bool checked)
{
	current = new Object();
	UpdateObject(current);
	if (current->Test())
	{
		if (obj_file->UpdateObject(current));
		changed = false;
		ui.object_list->setCurrentRow(-1);
	}
	else
	{
		delete current;
		current = 0;
	}
}

void	ObjEdit::Save()
{
	if (!CheckForSave())
		return;

	obj_file->Save();
}

void	ObjEdit::SaveAs()
{
	if (!CheckForSave())
		return;

	QString	file_name = QFileDialog::getSaveFileName(this, tr("Save File As..."),
		QString::fromStdString(current_path), tr("Object files (*.xob);;All files (*.*)"));
	if (!file_name.isNull())
	{
		obj_file->UpdateFileName(file_name.toStdString());
		obj_file->Save();
		QFileInfo	file_info(QString::fromStdString(obj_file->FileName()));
		current_path = (file_info.absoluteDir()).absolutePath().toStdString();
	}
}

void	ObjEdit::SetUpConnections()
{	// actions
	connect(about_action, SIGNAL(triggered()), this, SLOT(About()));
	connect(del_obj_action, SIGNAL(triggered()), this, SLOT(DeleteObject()));
	connect(exit_action, SIGNAL(triggered()), this, SLOT(Close()));
	connect(font_action, SIGNAL(triggered()), this, SLOT(Font()));
	connect(help_action, SIGNAL(triggered()), this, SLOT(Help()));
	connect(load_action, SIGNAL(triggered()), this, SLOT(Load()));
	connect(new_file_action, SIGNAL(triggered()), this, SLOT(NewFile()));
	connect(new_obj_action, SIGNAL(triggered()), this, SLOT(NewObject()));
	connect(save_action, SIGNAL(triggered()), this, SLOT(Save()));
	connect(saveas_action, SIGNAL(triggered()), this, SLOT(SaveAs()));
	connect(undo_action, SIGNAL(triggered()), this, SLOT(Undo()));

	// form components changed signals
	connect(ui.vocab_table, SIGNAL(cellChanged(int, int)), this, SLOT(OnTableChanged(int, int)));

	connect(ui.no_teleport_check, SIGNAL(stateChanged(int)), this, SLOT(OnButtonChanged(int)));
	connect(ui.persist_check, SIGNAL(stateChanged(int)), this, SLOT(OnButtonChanged(int)));
	connect(ui.visible_check, SIGNAL(stateChanged(int)), this, SLOT(OnButtonChanged(int)));

	connect(ui.desc_edit, SIGNAL(textChanged()), this, SLOT(OnFormChanged()));

	connect(ui.id_edit, SIGNAL(textEdited(const QString&)), this, SLOT(OnTextEdited(const QString&)));
	connect(ui.name_edit, SIGNAL(textEdited(const QString&)), this, SLOT(OnTextEdited(const QString&)));
	connect(ui.size_edit, SIGNAL(textEdited(const QString&)), this, SLOT(OnTextEdited(const QString&)));
	connect(ui.start_edit, SIGNAL(textEdited(const QString&)), this, SLOT(OnTextEdited(const QString&)));
	connect(ui.weight_edit, SIGNAL(textEdited(const QString&)), this, SLOT(OnTextEdited(const QString&)));

	connect(ui.definite_btn, SIGNAL(toggled(bool)), this, SLOT(OnButtonChanged(bool)));
	connect(ui.indef_btn, SIGNAL(toggled(bool)), this, SLOT(OnButtonChanged(bool)));
	connect(ui.no_art_btn, SIGNAL(toggled(bool)), this, SLOT(OnButtonChanged(bool)));
	connect(ui.plural_btn, SIGNAL(toggled(bool)), this, SLOT(OnButtonChanged(bool)));

	// others
	connect(ui.abstract_btn, SIGNAL(clicked(bool)), this, SLOT(OnNonStaticBtnClicked(bool)));
	connect(ui.add_btn, SIGNAL(clicked(bool)), this, SLOT(OnAddBtnClicked(bool)));
	connect(ui.dynamic_btn, SIGNAL(clicked(bool)), this, SLOT(OnNonStaticBtnClicked(bool)));
	connect(ui.static_btn, SIGNAL(clicked(bool)), this, SLOT(OnStaticBtnClicked(bool)));
	connect(ui.object_list, SIGNAL(itemSelectionChanged()), this, SLOT(OnObjectSelect()));
	connect(ui.update_btn, SIGNAL(clicked(bool)), this, SLOT(OnUpdateBtnClicked(bool)));
}

void	ObjEdit::SetUpTables()
{
	ui.vocab_table->setRowCount(TABLE_ROWS);
	ui.vocab_table->setColumnCount(2);
	ui.vocab_table->setColumnWidth(0, 120);
	ui.vocab_table->setColumnWidth(1, 120);
	for (int count = 0; count < TABLE_ROWS; ++count)
		ui.vocab_table->setRowHeight(count, 24);
	QStringList	header_list("Command");
	header_list.append("Event");
	ui.vocab_table->setHorizontalHeaderLabels(header_list);
	ui.vocab_table->setAlternatingRowColors(true);

	ui.move_table->setRowCount(TABLE_ROWS);
	ui.move_table->setColumnCount(5);
	for (int count = 0; count < 4; ++count)
		ui.move_table->setColumnWidth(count, 39);
	for (int count = 0; count < TABLE_ROWS; ++count)
		ui.move_table->setRowHeight(count, 24);
	ui.move_table->setColumnWidth(4, 38);
	ui.move_table->setAlternatingRowColors(true);
}

void	ObjEdit::Undo()
{
	obj_file->UndoDelete();
}

void	ObjEdit::UpdateObject(Object *object)
{
	object->Name(ui.name_edit->text().toStdString());
	object->Id(ui.id_edit->text().toStdString());
	object->Desc(ui.desc_edit->document()->toPlainText().toStdString());
	object->Size(ui.size_edit->text().toStdString());
	object->StartLoc(ui.start_edit->text().toStdString());
	object->Weight(ui.weight_edit->text().toStdString());

	if (ui.visible_check->checkState() == Qt::Checked)
		object->Visibility("100");
	else
		object->Visibility("0");
	if (ui.persist_check->checkState() == Qt::Checked)
		object->Property(Object::STORABLE, true);
	else
		object->Property(Object::STORABLE, false);
	if (ui.no_teleport_check->checkState() == Qt::Checked)
		object->Property(Object::NO_TP, true);
	else
		object->Property(Object::NO_TP, false);

	if (ui.definite_btn->isChecked())
		object->Property(Object::DEFINITE, true);
	else
		object->Property(Object::DEFINITE, false);
	if (ui.indef_btn->isChecked())
		object->Property(Object::INDEFINITE, true);
	else
		object->Property(Object::INDEFINITE, false);
	if (ui.plural_btn->isChecked())
		object->Property(Object::PLURAL, true);
	else
		object->Property(Object::PLURAL, false);

	if (ui.abstract_btn->isChecked())	object->ObjectType(Object::OB_ABSTRACT);
	if (ui.dynamic_btn->isChecked())	object->ObjectType(Object::OB_DYNAMIC);
	if (ui.mobile_btn->isChecked())	object->ObjectType(Object::OB_MOBILE);
	if (ui.static_btn->isChecked())	object->ObjectType(Object::OB_STATIC);

	QTableWidgetItem	*item;
	ObjVocab				*vocab;
	object->ClearVocab();
	for (int count = 0; count < TABLE_ROWS; ++count)
	{
		if ((ui.vocab_table->item(count, 0) == 0) || (ui.vocab_table->item(count, 1) == 0))
			break;

		vocab = new ObjVocab;
		vocab->command = ui.vocab_table->item(count, 0)->text().toStdString();
		vocab->event = ui.vocab_table->item(count, 1)->text().toStdString();
		object->AddVocab(vocab);
	}
}

void	ObjEdit::WriteStatusBar(const std::string& text)
{
	status_label->setText(QString::fromStdString(text));
}

