/*-----------------------------------------------------------------------
ObjEdit version 2.00 - Windows Federation 2 Object Editor
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef OBJEDIT_H
#define OBJEDIT_H

#include <QMainWindow>
#include <QAction>
#include <QMenu>

#include <map>
#include <string>

#include "ui_objedit.h"

class	ObjFile;
class	Object;

typedef	std::map<std::string, Object *, std::less<std::string> >	ObjectIndex;

class ObjEdit : public QMainWindow
{
	Q_OBJECT

private:
	static ObjEdit	*main_window;

	const static int	TABLE_ROWS;

	enum	{ COMMAND = 0, EVENT = 1 };	// define columns for vocavulary table

	Ui::ObjEditClass ui;

	ObjFile		*obj_file;
	Object		*current;
	std::string	version;
	bool			changed;
	std::string	current_path;
	ObjectIndex	index;						// Objects indexed by ID

	QAction	*about_action;
	QAction	*del_obj_action;
	QAction	*exit_action;
	QAction	*font_action;
	QAction	*help_action;
	QAction	*load_action;
	QAction	*new_file_action;
	QAction	*new_obj_action;
	QAction	*save_action;
	QAction	*saveas_action;
	QAction	*undo_action;

	QLabel	*status_label;

	QMenu		*edit_menu;
	QMenu		*file_menu;
	QMenu		*help_menu;

	bool	CheckForSave();

	void	closeEvent(QCloseEvent *event);
	void	CreateActions();
	void	CreateMainMenu();
	void	CreateStatusBar();
	void	SetUpConnections();
	void	SetUpTables();
	void	UpdateObject(Object *);

	private slots:
	void	About();
	void	DeleteObject();
	void	Close();
	void	Font();
	void	Help();
	void	Load();
	void	NewFile();
	void	NewObject();
	void	Save();
	void	Undo();

	void	OnAddBtnClicked(bool checked);
	void	OnNonStaticBtnClicked(bool checked);
	void	OnObjectSelect();
	void	OnStaticBtnClicked(bool checked);
	void	OnUpdateBtnClicked(bool checked);

	void	OnButtonChanged(bool)					{ changed = true; }
	void	OnButtonChanged(int)						{ changed = true; }
	void	OnFormChanged()							{ changed = true; }
	void	OnTableChanged(int, int)					{ changed = true; }
	void	OnTextEdited(const QString & text)	{ changed = true; }

	public slots:
	void	SaveAs();

public:
	static ObjEdit	*MainWindow()				{ return(main_window); }

	ObjEdit(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~ObjEdit();

	std::string&	Version()					{ return(version); }

	bool	AskAboutFileSave();

	void	AddObjectToList(const Object *object);
	void	ClearObjectDisplay();
	void	ClearObjectList();
	void	Display(Object *object);
	void	DisplayObjectList(const std::map<std::string, Object *, std::less<std::string> >& index);
	void	EnableSave(bool new_status)		{ save_action->setEnabled(new_status); }
	void	EnableUndo(bool new_status)		{ undo_action->setEnabled(new_status); }
	void	Error(const std::string& text);
	void	Message(const std::string& text);
	void	WriteStatusBar(const std::string& text);
};

#endif // OBJEDIT_H

