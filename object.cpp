/*-----------------------------------------------------------------------
ObjEdit version 2.00 - Windows Federation 2 Object Editor
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "object.h"

#include <sstream>

#include <cctype>

#include "objedit.h"


const std::string	Object::type_names[] = { "abstract", "dynamic", "mobile", "static" };

Object::Object() : object_type(OB_STATIC), start_loc("0"), visibility("100"), size("1"), weight("1")
{
	properties.set(STORABLE);
}

Object::Object(const QXmlAttributes& attribs)
{
	name = id = desc = "xxx";
	std::string	temp;
	GetAttribute(attribs, "type", temp);
	object_type = GetType(temp);

	GetAttribute(attribs, "start", start_loc);
	GetAttribute(attribs, "visibility", visibility);
	GetAttribute(attribs, "size", size);
	GetAttribute(attribs, "weight", weight);

	temp = "";
	GetAttribute(attribs, "flags", temp);
	GetFlags(temp);
}

Object::~Object()
{
	for (VocabList::iterator iter = vocab_list.begin(); iter != vocab_list.end(); ++iter)
		delete *iter;
	vocab_list.clear();
}

void	Object::AddVocab(const QXmlAttributes &attribs)
{
	ObjVocab	*vocab = new ObjVocab;
	GetAttribute(attribs, "cmd", vocab->command);
	GetAttribute(attribs, "event", vocab->event);
	vocab_list.push_back(vocab);
}

void	Object::Display()
{
	ObjEdit::MainWindow()->Display(this);
}

std::string&	Object::EscapeXML(std::string& text)
{
	std::ostringstream	buffer;
	int	len = text.length();
	for (int count = 0; count <len; ++count)
	{
		switch (text[count])
		{
		case  '<':	buffer << "&lt;";		break;
		case  '>':	buffer << "&gt;";		break;
		case  '&':	buffer << "&amp;";	break;
		case '\'':	buffer << "&apos;";	break;
		case '\"':	buffer << "&quot;";	break;
		default:	if (isascii(text[count]) == 0)
			buffer << '#';
					else
						buffer << text[count];
			break;
		}
	}
	text = buffer.str();
	return(text);
}

void	Object::GetAttribute(const QXmlAttributes &attribs,
	const std::string& name, std::string& value)
{
	value = (attribs.value(QString::fromStdString(name))).toStdString();
}

void	Object::GetFlags(const std::string& text)
{
	int	len = text.length();
	if (len > 0)
	{
		for (int count = 0; count < len; ++count)
		{
			switch (text[count])
			{
			case 'd':	properties.set(DEFINITE);		break;
			case 'i':	properties.set(INDEFINITE);	break;
			case 'p':	properties.set(PLURAL);			break;
			case 's':	properties.set(STORABLE);		break;
			case 't':	properties.set(NO_TP);			break;
			}
		}
	}
}

int	Object::GetType(const std::string& text)
{
	int type = OB_STATIC;
	for (int count = 0; count < MAX_TYPES; ++count)
	{
		if (type_names[count] == text)
		{
			type = count;
			break;
		}
	}
	return(type);
}

void	Object::Property(int which, bool setting)
{
	if (setting)
		properties.set(which);
	else
		properties.reset(which);
}

void	Object::Save(std::ofstream& file)
{
	const char flag_names[] = { 'd', 'i', 'p', 's', 't' };

	file << "   <object type='" << type_names[object_type];
	file << "' start='" << start_loc << "' visibility='" << visibility;
	file << "' size='" << size << "' weight='" << weight << "'";
	if (properties.any())
	{
		file << " flags='";
		for (int count = 0; count < MAX_PROPS; ++count)
		{
			if (properties.test(count))
				file << flag_names[count];
		}
		file << "'";
	}
	file << ">\n";

	std::string	xml_name(name), xml_id(id), xml_desc(desc);
	file << "      <name>" << EscapeXML(xml_name) << "</name>\n";
	file << "      <id>" << EscapeXML(xml_id) << "</id>\n";
	file << "      <desc>" << EscapeXML(xml_desc) << "</desc>\n";

	if (vocab_list.size() > 0)
	{
		for (VocabList::iterator iter = vocab_list.begin(); iter != vocab_list.end(); ++iter)
		{
			file << "      <vocab cmd='" << (*iter)->command;
			file << "' event='" << (*iter)->event << "'></vocab>\n";
		}
	}
	file << "   </object>\n\n";
}

bool	Object::Test()
{
	if (name.size() < 2)
	{
		ObjEdit::MainWindow()->Error("Object names must be at least two characters long!");
		return(false);
	}
	if (id == "")
	{
		ObjEdit::MainWindow()->Error("You haven't provided an object ID!");
		return(false);
	}
	if (desc == "")
	{
		ObjEdit::MainWindow()->Error("You haven't provided a description!");
		return(false);
	}
	if ((!std::isdigit(size[0])) || (std::atoi(size.c_str()) < 1))
	{
		ObjEdit::MainWindow()->Error("'Size' must be a number, and greater than zero!");
		return(false);
	}
	if ((!std::isdigit(weight[0])) || (std::atoi(weight.c_str()) < 1))
	{
		ObjEdit::MainWindow()->Error("'Weight' must be a number, and greater than zero!");
		return(false);
	}
	if ((!std::isdigit(start_loc[0])) || (std::atoi(start_loc.c_str()) < 0)
		|| (std::atoi(start_loc.c_str()) > 4095))
	{
		ObjEdit::MainWindow()->Error("'Start Location' must be a number\nbetween zero and 4095 inclusive!");
		return(false);
	}
	return(true);
}

