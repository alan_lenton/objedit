/*-----------------------------------------------------------------------
ObjEdit version 2.00 - Windows Federation 2 Object Editor
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef OBJECT_H
#define OBJECT_H

#include <bitset>
#include <fstream>
#include <list>
#include <string>

#include <QXmlAttributes>


class	ObjEdit;

struct	ObjVocab
{
	std::string	command;
	std::string	event;
};

typedef	std::list<ObjVocab *>	VocabList;


class	Object
{
public:
	enum	{ OB_ABSTRACT, OB_DYNAMIC, OB_MOBILE, OB_STATIC, MAX_TYPES };	//	object types
	enum	{ DEFINITE, INDEFINITE, PLURAL, STORABLE, NO_TP, MAX_PROPS };	// properties

private:
	static const std::string	type_names[];

	int	object_type;

	std::string	name;				// object's (non-unique) name
	std::string id;				// object's id (unique within map)
	std::string	desc;
	std::string	start_loc;
	std::string	visibility;		// percentage - 100% fully visible
	std::string	size;
	std::string	weight;

	std::bitset<MAX_PROPS>	properties;
	VocabList	vocab_list;

	std::string&	CleanText(std::string text);
	std::string&	EscapeXML(std::string& text);

	int	GetType(const std::string& text);

	void	GetAttribute(const QXmlAttributes &attribs, const std::string& name, std::string& value);
	void	GetFlags(const std::string& text);

public:
	Object();
	Object(const QXmlAttributes &attribs);
	~Object();

	// getters
	const VocabList&		Vocab() const			{ return(vocab_list); }
	const std::string&	Desc() const			{ return(desc); }
	const std::string&	Id() const				{ return(id); }
	const std::string&	Name() const			{ return(name); }
	const std::string&	Size() const			{ return(size); }
	const std::string&	StartLoc() const		{ return(start_loc); }
	const std::string&	Visibility() const	{ return(visibility); }
	const std::string&	Weight() const			{ return(weight); }
	int	ObjectType() const						{ return(object_type); }
	bool	Property(int which) const				{ return(properties.test(which)); }

	// setters
	void	AddVocab(ObjVocab	*vocab)				{ vocab_list.push_back(vocab); }
	void	ClearVocab()								{ vocab_list.clear(); }
	void	Desc(const std::string& text)			{ desc = text; }
	void	Id(const std::string& text)			{ id = text; }
	void	Name(const std::string& text)			{ name = text; }
	void	ObjectType(int new_type)				{ object_type = new_type; }
	void	Property(int which, bool setting);
	void	Size(const std::string& text)			{ size = text; }
	void	StartLoc(const std::string& text)	{ start_loc = text; }
	void	Visibility(const std::string& text)	{ visibility = text; }
	void	Weight(const std::string& text)		{ weight = text; }

	// the rest
	bool	Test();
	void	AddVocab(const QXmlAttributes &attribs);
	void	Display();
	void	Save(std::ofstream& file);
};

#endif
