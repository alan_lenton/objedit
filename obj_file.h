/*-----------------------------------------------------------------------
ObjEdit version 2.00 - Windows Federation 2 Object Editor
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef OBJFILE_H
#define OBJFILE_H

#include <map>

#include <QXmlSimpleReader>


class	Object;

typedef std::map<std::string, Object *, std::less<std::string> >	ObjIndex;

class	ObjFile : public QXmlDefaultHandler
{
private:
	static const int	NOT_FOUND;

	enum	{ OBF_LIST, OBF_OBJECT, OBF_NAME, OBF_ID, OBF_DESC, OBF_VOCAB };

	ObjIndex	index;			// objects indexed by ID
	std::string	file_name;
	std::string	buffer;		// holds characters() text
	int	version;
	bool	changed;

	Object	*current;		// only used for loading files
	Object	*last_delete;

	Object	*Find(const std::string& id);

	int	FindElement(const std::string& element);

	// These three functions are part of Qt's XML parsing operation	
	bool	characters(const QString& text);
	bool	endElement(const QString& namespaceURI, const QString& localName, const QString& qName);
	bool	startElement(const QString& namespaceURI, const QString& localName,
		const QString& qName, const QXmlAttributes& atts);

	void	EndDesc();
	void	EndId();
	void	EndList();
	void	EndName();
	void	EndObject();
	void	StartObject(const QXmlAttributes& attribs);
	void	StartObjectList(const QXmlAttributes& attribs);
	void	StartVocab(const QXmlAttributes& attribs);

public:
	ObjFile(int vers = 0) : version(vers), changed(false), current(0), last_delete(0)	{	}
	~ObjFile();

	std::string&	FileName()							{ return(file_name); }
	bool	AddObject(Object *object);
	bool	CanClose()										{ return(!changed); }
	bool	UpdateObject(Object *object);

	void	Changed(bool new_status)					{ changed = new_status; }
	void	Clear();
	void	ListSelected(const std::string& obj_id);
	void	Load(const std::string& f_name);
	void	NewFile();
	void	RemoveObject(const std::string& id);
	void	UndoDelete();
	void	UpdateFileName(const std::string& f_name);
	void	Save();
};

#endif

