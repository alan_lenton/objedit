/*-----------------------------------------------------------------------
ObjEdit version 2.00 - Windows Federation 2 Object Editor
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Program compiled with Visual Studio 2010 and Qt4.8.4
Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include	"obj_file.h"

#include <fstream>
#include <sstream>

#include <QFile>

#include "object.h"
#include "objedit.h"


const int	ObjFile::NOT_FOUND = 999;

ObjFile::~ObjFile()
{
	for (ObjIndex::iterator iter = index.begin(); iter != index.end(); ++iter)
		delete iter->second;
	index.clear();
	delete last_delete;
}


bool	ObjFile::AddObject(Object *object)
{
	if (Find(object->Id()) == 0)
	{
		index[object->Id()] = object;
		ObjEdit::MainWindow()->AddObjectToList(object);
		ObjEdit::MainWindow()->ClearObjectDisplay();
		changed = true;
	}
	else
	{
		std::ostringstream	buffer;
		buffer << "Object ID: " << object->Id() << " already exists!\nPlease use the 'Update Object' button.";
		ObjEdit::MainWindow()->Message(buffer.str());
		return(false);
	}

	return(true);
}

void	ObjFile::Clear()
{
	version = 0;
	changed = false;
	current = 0;
	file_name = "";
	buffer = "";

	for (ObjIndex::iterator iter = index.begin(); iter != index.end(); ++iter)
		delete iter->second;
	index.clear();
	changed = false;
}

Object	*ObjFile::Find(const std::string& id)
{
	ObjIndex::iterator	iter = index.find(id);
	if (iter != index.end())
		return(iter->second);
	else
		return(0);
}

void	ObjFile::ListSelected(const std::string& obj_id)
{
	ObjIndex::iterator	iter = index.find(obj_id);
	if (iter != index.end())
	{
		current = iter->second;
		current->Display();
	}
	// not finding it is not an error - this function gets triggered when we delete an object
}

void	ObjFile::NewFile()
{
	if (changed)
	{
		if (!ObjEdit::MainWindow()->AskAboutFileSave())
			return;
	}

	Clear();
	ObjEdit::MainWindow()->ClearObjectDisplay();
	ObjEdit::MainWindow()->ClearObjectList();
	ObjEdit::MainWindow()->EnableSave(false);
	changed = false;
}

void	ObjFile::RemoveObject(const std::string& id)
{
	ObjIndex::iterator iter = index.find(id);
	if (iter != index.end())
	{
		delete last_delete;
		last_delete = iter->second;
		index.erase(iter);
		ObjEdit::MainWindow()->ClearObjectDisplay();
		ObjEdit::MainWindow()->DisplayObjectList(index);
		ObjEdit::MainWindow()->EnableUndo(true);
		changed = true;
	}
}

void	ObjFile::Save()
{
	if (file_name == "")
	{
		ObjEdit::MainWindow()->SaveAs();
		return;
	}

	std::ofstream	file(file_name.c_str(), std::ios::out | std::ios::trunc);
	if (!file)
	{
		ObjEdit::MainWindow()->Error("Unable to open file for writing!");
		return;
	}

	file << "<?xml version=\"1.0\"?>\n\n";
	file << "<object-list version='" << ++version << "' editor='" << ObjEdit::MainWindow()->Version() << "'>\n";
	for (ObjIndex::iterator iter = index.begin(); iter != index.end(); ++iter)
		iter->second->Save(file);
	file << "</object-list>\n\n";

	changed = false;
	ObjEdit::MainWindow()->EnableSave(true);	// we might have got here via 'Save As...'
	ObjEdit::MainWindow()->Message("File saved to disk");
}


void	ObjFile::UndoDelete()
{
	if (last_delete == 0)
		ObjEdit::MainWindow()->Error("Unable to find last delete!");
	else
	{
		AddObject(last_delete);
		last_delete = 0;
		ObjEdit::MainWindow()->EnableUndo(false);
		changed = true;
	}
}

void	ObjFile::UpdateFileName(const std::string& f_name)
{
	file_name = f_name;
	ObjEdit::MainWindow()->WriteStatusBar(file_name);
	changed = true;
}

bool	ObjFile::UpdateObject(Object *object)
{
	ObjIndex::iterator	iter = index.find(object->Id());
	if (iter != index.end())
	{
		Object *temp = iter->second;
		index.erase(iter);
		delete temp;
		index[object->Id()] = object;
		ObjEdit::MainWindow()->ClearObjectDisplay();
		changed = true;
	}
	else
	{
		ObjEdit::MainWindow()->Message("That object ID does not exist, please\nuse the 'Add Object' button");
		return(false);
	}

	return(true);
}



/*-------------------------------------------------------
All the functions and static variables relating solely
to loading and parsing the file are located below - AL
-------------------------------------------------------*/

bool	ObjFile::characters(const QString& text)
{
	buffer += text.toStdString();
	return(true);
}

void	ObjFile::EndDesc()
{
	if (current != 0)
		current->Desc(buffer);
}

bool	ObjFile::endElement(const QString& namespaceURI, const QString& localName, const QString& qName)
{
	switch (FindElement(qName.toStdString()))
	{
	case OBF_LIST:		EndList();		break;
	case OBF_OBJECT:	EndObject();	break;
	case OBF_NAME:		EndName();		break;
	case OBF_ID:		EndId();			break;
	case OBF_DESC:		EndDesc();		break;
	}
	return(true);
}

void	ObjFile::EndId()
{
	if (current != 0)
		current->Id(buffer);
}

void	ObjFile::EndList()
{
	changed = false;
	ObjEdit::MainWindow()->DisplayObjectList(index);
}

void	ObjFile::EndName()
{
	if (current != 0)
		current->Name(buffer);
}

void	ObjFile::EndObject()
{
	AddObject(current);
	current = 0;
}

int	ObjFile::FindElement(const std::string& element)
{
	static const std::string	el_names[] =
	{ "object-list", "object", "name", "id", "desc", "vocab", "" };

	for (int which = 0; el_names[which] != ""; ++which)
	{
		if (element == el_names[which])
			return(which);
	}
	return(NOT_FOUND);
}

void	ObjFile::Load(const std::string& f_name)
{
	if (changed)
	{
		if (!ObjEdit::MainWindow()->AskAboutFileSave())
			return;
	}

	Clear();
	QFile	file(QString::fromStdString(f_name));
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		file_name = f_name;
		QXmlInputSource	source(&file);
		QXmlSimpleReader	reader;
		reader.setContentHandler(this);
		reader.parse(source);
		file.close();
		ObjEdit::MainWindow()->WriteStatusBar(file_name);
		changed = false;
		ObjEdit::MainWindow()->EnableSave(true);
	}
	else
		ObjEdit::MainWindow()->Error("Unable to open file for reading!");
}

bool	ObjFile::startElement(const QString& namespaceURI, const QString& localName,
	const QString& qName, const QXmlAttributes& atts)
{
	switch (FindElement(qName.toStdString()))
	{
	case OBF_LIST:		StartObjectList(atts);	break;
	case OBF_OBJECT:	StartObject(atts);		break;
	case OBF_NAME:		// name - drop through
	case OBF_ID:		// id - drop through
	case OBF_DESC:		buffer = "";				break;	// name/id/desc init buffer for character collection
	case OBF_VOCAB:	StartVocab(atts);			break;
	}
	return(true);
}

void	ObjFile::StartObject(const QXmlAttributes& attribs)
{
	current = new Object(attribs);
}

void	ObjFile::StartObjectList(const QXmlAttributes& attribs)
{
	version = attribs.value(QString::fromLatin1("version")).toInt();
}

void	ObjFile::StartVocab(const QXmlAttributes& attribs)
{
	if (current != 0)
		current->AddVocab(attribs);
}

